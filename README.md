# Folder Structure Oprisk 1

```sh
├───modules
│   └───rsca
│       ├───maintance
│       │   ├───modals
│       │   │   ├───add-ketentuan
│       │   │   ├───add-key-control       
│       │   │   ├───add-key-process       
│       │   │   ├───add-key-risk
│       │   │   ├───add-let
│       │   │   ├───add-risk-library      
│       │   │   ├───edit-ketentuan        
│       │   │   ├───edit-key-control      
│       │   │   └───setting-ratting-modals
│       │   │       ├───add-impact
│       │   │       ├───edit-control
│       │   │       ├───edit-ihrr-predicate   
│       │   │       ├───edit-impact
│       │   │       ├───edit-likelihood       
│       │   │       └───edit-ratting-composite
│       │   └───tabs
│       │       ├───ketentuan
│       │       ├───key-control
│       │       ├───key-process
│       │       │   ├───jaringan
│       │       │   └───kantor-pusat
│       │       ├───key-risk
│       │       │   ├───jaringan
│       │       │   └───kantor-pusat
│       │       ├───let
│       │       ├───risk-library
│       │       │   ├───kelola-risk-library
│       │       │   └───risk-register
│       │       ├───setting-rating
│       │       │   ├───control
│       │       │   ├───ihrr-predicate
│       │       │   ├───impact
│       │       │   ├───likelihood
│       │       │   └───ratting-composite
│       │       ├───setting-top-risk
│       │       └───top-risk
│       └───user-managment
│           ├───modals
│           │   ├───add-role
│           │   ├───edit-role
│           │   ├───role
│           │   └───unit-kerja-modals
│           │       ├───jaringan
│           │       │   ├───add-area
│           │       │   ├───add-cabang
│           │       │   ├───add-region
│           │       │   ├───add-segmen
│           │       │   ├───edit-area
│           │       │   ├───edit-cabang
│           │       │   ├───edit-region
│           │       │   └───edit-segmen
│           │       └───kantor-pusat
│           │           ├───add-department
│           │           ├───add-directorate
│           │           ├───add-group
│           │           ├───edit-department
│           │           ├───edit-directorate
│           │           └───edit-group
│           ├───role
│           │   ├───market-risk
│           │   ├───opearional-risk
│           │   └───risk-integration
│           ├───unit-kerja
│           │   ├───jaringan
│           │   │   ├───area
│           │   │   ├───cabang
│           │   │   ├───region
│           │   │   └───segmen
│           │   └───kantor-pusat
│           │       ├───department
│           │       ├───directorate
│           │       └───group
│           ├───user
│           └───user-delegate
├───services
│   └───rsca
│       ├───maintance
│       │   ├───ketentuan
│       │   ├───key-control
│       │   ├───key-process
│       │   ├───key-risk
│       │   ├───let
│       │   ├───risk-library
│       │   ├───setting-rating
│       │   ├───setting-top-risk
│       │   └───top-risk
│       └───user-managment
│           ├───role
│           ├───unit-kerja
│           ├───user
│           └───user-delegate
└───state
    └───rcsa
        ├───maintance
        │   ├───ketentuan
        │   │   ├───effect
        │   │   └───store
        │   │       ├───action
        │   │       └───reducer
        │   ├───key-control
        │   │   ├───effect
        │   │   └───store
        │   │       ├───action
        │   │       └───reducer
        │   ├───key-process
        │   │   ├───effect
        │   │   └───store
        │   │       ├───action
        │   │       └───reducer
        │   ├───key-risk
        │   │   ├───effect
        │   │   └───store
        │   │       ├───action
        │   │       └───reducer
        │   ├───let
        │   │   ├───effect
        │   │   └───store
        │   │       ├───action
        │   │       └───reducer
        │   ├───risk-library
        │   │   ├───effect
        │   │   └───store
        │   │       ├───action
        │   │       └───reducer
        │   ├───setting-rating
        │   │   ├───effect
        │   │   └───store
        │   │       ├───action
        │   │       └───reducer
        │   ├───setting-top-risk
        │   │   ├───effect
        │   │   └───store
        │   │       ├───action
        │   │       └───reducer
        │   └───top-risk
        │       ├───effect
        │       └───store
        │           ├───action
        │           └───reducer
        └───user-managment
            ├───role
            │   ├───effect
            │   └───store
            │       ├───action
            │       └───reducer
            ├───unit-kerja
            │   ├───effect
            │   └───store
            │       ├───action
            │       └───reducer
            ├───user
            │   ├───effect
            │   └───store
            │       ├───action
            │       └───reducer
            └───user-delegate
                ├───effect
                └───store
                    ├───action
                    └───reducer
```


### Detail folder
```sh
├───modules
│   └───rsca
│       ├───maintance
│       │   ├───modals
│       │   │   ├───add-ketentuan
│       │   │   │       filename.component.html
│       │   │   │       filename.component.ts  
│       │   │   │
│       │   │   ├───add-key-control
│       │   │   │       filename.component.html
│       │   │   │       filename.component.ts  
│       │   │   │
│       │   │   ├───add-key-process
│       │   │   │       filename.component.html
│       │   │   │       filename.component.ts  
│       │   │   │
│       │   │   ├───add-key-risk
│       │   │   │       filename.component.html
│       │   │   │       filename.component.ts  
│       │   │   │
│       │   │   ├───add-let
│       │   │   │       filename.component.html
│       │   │   │       filename.component.ts
│       │   │   │
│       │   │   ├───add-risk-library
│       │   │   │       filename.component.html
│       │   │   │       filename.component.ts
│       │   │   │
│       │   │   ├───edit-ketentuan
│       │   │   │       filename.component.html
│       │   │   │       filename.component.ts
│       │   │   │
│       │   │   ├───edit-key-control
│       │   │   │       filename.component copy.html
│       │   │   │       filename.component copy.ts
│       │   │   │       filename.component.html
│       │   │   │       filename.component.ts
│       │   │   │
│       │   │   └───setting-ratting-modals
│       │   │       ├───add-impact
│       │   │       ├───edit-control
│       │   │       ├───edit-ihrr-predicate
│       │   │       ├───edit-impact
│       │   │       ├───edit-likelihood
│       │   │       └───edit-ratting-composite
│       │   └───tabs
│       │       ├───ketentuan
│       │       │       filename.component.html
│       │       │       filename.component.ts
│       │       │
│       │       ├───key-control
│       │       │       filename.component.html
│       │       │       filename.component.ts
│       │       │
│       │       ├───key-process
│       │       │   ├───jaringan
│       │       │   │       filename.component.html
│       │       │   │       filename.component.ts
│       │       │   │
│       │       │   └───kantor-pusat
│       │       │           filename.component.html
│       │       │           filename.component.ts
│       │       │
│       │       ├───key-risk
│       │       │   ├───jaringan
│       │       │   │       filename.component.html
│       │       │   │       filename.component.ts
│       │       │   │
│       │       │   └───kantor-pusat
│       │       │           filename.component.html
│       │       │           filename.component.ts
│       │       │
│       │       ├───let
│       │       │       filename.component.html
│       │       │       filename.component.ts
│       │       │
│       │       ├───risk-library
│       │       │   ├───kelola-risk-library
│       │       │   │       filename.component.html
│       │       │   │       filename.component.ts
│       │       │   │
│       │       │   └───risk-register
│       │       │           filename.component.html
│       │       │           filename.component.ts
│       │       │
│       │       ├───setting-rating
│       │       │   ├───control
│       │       │   │       filename.component.html
│       │       │   │       filename.component.ts
│       │       │   │
│       │       │   ├───ihrr-predicate
│       │       │   │       filename.component.html
│       │       │   │       filename.component.ts
│       │       │   │
│       │       │   ├───impact
│       │       │   │       filename.component.html
│       │       │   │       filename.component.ts
│       │       │   │       
│       │       │   ├───likelihood
│       │       │   │       filename.component.html
│       │       │   │       filename.component.ts
│       │       │   │
│       │       │   └───ratting-composite
│       │       │           filename.component.html
│       │       │           filename.component.ts
│       │       │
│       │       ├───setting-top-risk
│       │       │       filename.component.html
│       │       │       filename.component.ts
│       │       │
│       │       └───top-risk
│       │               filename.component.html
│       │               filename.component.ts
│       │
│       └───user-managment
│           ├───modals
│           │   ├───add-role
│           │   ├───edit-role
│           │   ├───role
│           │   └───unit-kerja-modals
│           │       ├───jaringan
│           │       │   ├───add-area
│           │       │   ├───add-cabang
│           │       │   ├───add-region
│           │       │   ├───add-segmen
│           │       │   ├───edit-area
│           │       │   ├───edit-cabang
│           │       │   ├───edit-region
│           │       │   └───edit-segmen
│           │       └───kantor-pusat
│           │           ├───add-department
│           │           ├───add-directorate
│           │           ├───add-group
│           │           ├───edit-department
│           │           ├───edit-directorate
│           │           └───edit-group
│           ├───role
│           │   ├───market-risk
│           │   ├───opearional-risk
│           │   └───risk-integration
│           ├───unit-kerja
│           │   ├───jaringan
│           │   │   ├───area
│           │   │   ├───cabang
│           │   │   ├───region
│           │   │   └───segmen
│           │   └───kantor-pusat
│           │       ├───department
│           │       ├───directorate
│           │       └───group
│           ├───user
│           └───user-delegate
├───services
│   └───rsca
│       ├───maintance
│       │   ├───ketentuan
│       │   │       filename.service.ts
│       │   │
│       │   ├───key-control
│       │   │       filename.service.ts
│       │   │
│       │   ├───key-process
│       │   │       filename.service.ts
│       │   │
│       │   ├───key-risk
│       │   │       filename.service.ts
│       │   │
│       │   ├───let
│       │   │       filename.service.ts
│       │   │
│       │   ├───risk-library
│       │   │       filename.service.ts
│       │   │
│       │   ├───setting-rating
│       │   │       filename.service.ts
│       │   │
│       │   ├───setting-top-risk
│       │   │       filename.service.ts
│       │   │
│       │   └───top-risk
│       │           filename.service.ts
│       │
│       └───user-managment
│           ├───role
│           │       filename.service.ts
│           │
│           ├───unit-kerja
│           │       filename.service.ts
│           │
│           ├───user
│           │       filename.service.ts
│           │
│           └───user-delegate
│                   filename.service.ts
│
└───state
    └───rcsa
        ├───maintance
        │   ├───ketentuan
        │   │   ├───effect
        │   │   │       filename.effect.ts
        │   │   │
        │   │   └───store
        │   │       ├───action
        │   │       │       filename.action.ts
        │   │       │
        │   │       └───reducer
        │   │               filename.reducer.ts
        │   │
        │   ├───key-control
        │   │   ├───effect
        │   │   │       filename.effect.ts
        │   │   │
        │   │   └───store
        │   │       ├───action
        │   │       │       filename.action.ts
        │   │       │
        │   │       └───reducer
        │   │               filename.reducer.ts
        │   │
        │   ├───key-process
        │   │   ├───effect
        │   │   │       filename.effect.ts
        │   │   │
        │   │   └───store
        │   │       ├───action
        │   │       │       filename.action.ts
        │   │       │
        │   │       └───reducer
        │   │               filename.reducer.ts
        │   │
        │   ├───key-risk
        │   │   ├───effect
        │   │   │       filename.effect.ts
        │   │   │
        │   │   └───store
        │   │       ├───action
        │   │       │       filename.action.ts
        │   │       │
        │   │       └───reducer
        │   │               filename.reducer.ts
        │   │
        │   ├───let
        │   │   ├───effect
        │   │   │       filename.effect.ts
        │   │   │
        │   │   └───store
        │   │       ├───action
        │   │       │       filename.action.ts
        │   │       │
        │   │       └───reducer
        │   │               filename.reducer.ts
        │   │
        │   ├───risk-library
        │   │   ├───effect
        │   │   │       filename.effect.ts
        │   │   │
        │   │   └───store
        │   │       ├───action
        │   │       │       filename.action.ts
        │   │       │
        │   │       └───reducer
        │   │               filename.reducer.ts
        │   │
        │   ├───setting-rating
        │   │   ├───effect
        │   │   │       filename.effect.ts
        │   │   │
        │   │   └───store
        │   │       ├───action
        │   │       │       filename.action.ts
        │   │       │
        │   │       └───reducer
        │   │               filename.reducer.ts
        │   │
        │   ├───setting-top-risk
        │   │   ├───effect
        │   │   │       filename.effect.ts
        │   │   │
        │   │   └───store
        │   │       ├───action
        │   │       │       filename.action.ts
        │   │       │
        │   │       └───reducer
        │   │               filename.reducer.ts
        │   │
        │   └───top-risk
        │       ├───effect
        │       │       filename.effect.ts
        │       │
        │       └───store
        │           ├───action
        │           │       filename.action.ts
        │           │
        │           └───reducer
        │                   filename.reducer.ts
        │
        └───user-managment
            ├───role
            │   ├───effect
            │   │       filename.effect.ts
            │   │
            │   └───store
            │       ├───action
            │       │       filename.action.ts
            │       │
            │       └───reducer
            │               filename.reducer.ts
            │
            ├───unit-kerja
            │   ├───effect
            │   │       filename.effect.ts
            │   │
            │   └───store
            │       ├───action
            │       │       filename.action.ts
            │       │
            │       └───reducer
            │               filename.reducer.ts
            │
            ├───user
            │   ├───effect
            │   │       filename.effect.ts
            │   │
            │   └───store
            │       ├───action
            │       │       filename.action.ts
            │       │
            │       └───reducer
            │               filename.reducer.ts
            │
            └───user-delegate
                ├───effect
                │       filename.effect.ts
                │
                └───store
                    ├───action
                    │       filename.action.ts
                    │
                    └───reducer
                            filename.reducer.ts
```

